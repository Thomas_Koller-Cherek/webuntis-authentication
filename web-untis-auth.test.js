const WebUntisAuth = require("./web-untis-auth");

describe("Unit tests for the WebUntis Authentication Service", () => {
    test("Testing login", async () => {
        expect(await WebUntisAuth.login("mia.theiler", "Test1234!")).toBeTruthy();
        expect(await WebUntisAuth.login("non-existant-user", "abc")).toBeFalsy();
        expect(await WebUntisAuth.login("non-existant-user")).toBeFalsy();
        expect(await WebUntisAuth.login("thomas.koller", "Test1234!")).toBeTruthy();
        expect(await WebUntisAuth.login("BonOnline_Admin", "UvN@Awb6j5")).toBeTruthy();
        expect(await WebUntisAuth.login("MaxmuLehrer", "Test1234!")).toBeTruthy();
    });

    test("Testing getting session token", async () => {
        expect(WebUntisAuth.getSessionToken("mia.theiler")).toBeTruthy();
        expect(WebUntisAuth.getSessionToken("non-existant-user")).toBeFalsy();
        expect(WebUntisAuth.getSessionToken("MaxmuLehrer")).toBeTruthy();
    });

    test("Testing getting user info", async () => {
        const mia = await WebUntisAuth.getUserInfo("mia.theiler");
        expect(mia).toBeTruthy();
        expect(mia.sessionId).toBeTruthy();
        expect(mia.personType).toBe(WebUntisAuth.TYPES.STUDENT);
        expect(mia.personId).toBe(37);
        expect(mia.klasseId).toBe(0);

        const BonOnline_AdminInfo = await WebUntisAuth.getUserInfo("BonOnline_Admin");
        expect(BonOnline_AdminInfo).toBeTruthy();
        expect(BonOnline_AdminInfo.sessionId).toBeTruthy();
        expect(BonOnline_AdminInfo.personType).toBe(WebUntisAuth.TYPES.ADMIN);
        expect(BonOnline_AdminInfo.personId).toBe(-1);
        expect(BonOnline_AdminInfo.klasseId).toBe(0);

        const MaxmuLehrerInfo = await WebUntisAuth.getUserInfo("MaxmuLehrer");
        expect(MaxmuLehrerInfo).toBeTruthy();
        expect(MaxmuLehrerInfo.sessionId).toBeTruthy();
        expect(MaxmuLehrerInfo.personType).toBe(WebUntisAuth.TYPES.TEACHER);
        expect(MaxmuLehrerInfo.personId).toBe(782);
        expect(MaxmuLehrerInfo.klasseId).toBe(0);
    });

    test("Testing getting fullname of user", async () => {
        expect(await WebUntisAuth.getUserFullName("mia.theiler")).toBe("Mia Theiler");
        expect(await WebUntisAuth.getUserFullName("non-existant-user")).toBe(undefined);
        expect(await WebUntisAuth.getUserFullName("MaxmuLehrer")).toBe("Max Mustermann");
    });

    test("Testing getting e-mail of user", async () => {
        expect(await WebUntisAuth.getUserEmail("mia.theiler")).toBe("mia.theiler@htlpinkafeld.at");
        expect(await WebUntisAuth.getUserEmail("non-existant-user")).toBe(undefined);
        expect(await WebUntisAuth.getUserEmail("MaxmuLehrer")).toBe("testmail@htlpinkafeld.at");
    });

    test("Testing getting gender of user", async () => {
        expect(await WebUntisAuth.getGender("mia.theiler")).toBe("female");
        expect(await WebUntisAuth.getGender("thomas.koller")).toBe("male");
        expect(await WebUntisAuth.getGender("non-existant-user")).toBe(undefined);
        expect(await WebUntisAuth.getGender("BonOnline_Admin")).toBe(undefined);
        expect(await WebUntisAuth.getGender("MaxmuLehrer")).toBe("divers");
    });

    test("Testing getting username by Session ID", async () => {
        const miaUserInfo = WebUntisAuth.getUserInfo("mia.theiler");
        expect(WebUntisAuth.getUsernameFromSessionID(miaUserInfo.sessionId)).toBe("mia.theiler");
        const MaxmuLehrerInfo = WebUntisAuth.getUserInfo("MaxmuLehrer");
        expect(WebUntisAuth.getUsernameFromSessionID(MaxmuLehrerInfo.sessionId)).toBe("MaxmuLehrer");
        const nonExistantSessionId = "abc";
        expect(WebUntisAuth.getUsernameFromSessionID(nonExistantSessionId)).toBe(undefined);
    });

    test("Testing logout", async () => {
        expect(await WebUntisAuth.logout("mia.theiler")).toBeTruthy();
        expect(await WebUntisAuth.logout("non-existant-user")).toBeFalsy();
        expect(await WebUntisAuth.logout("thomas.koller")).toBeTruthy();
        expect(await WebUntisAuth.logout("BonOnline_Admin")).toBeTruthy();
        expect(await WebUntisAuth.logout("MaxmuLehrer")).toBeTruthy();
    });
});