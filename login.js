let saveUsername = null;

const baseUrl = "http://localhost:3001";
const loginBtn = document.getElementById("loginBtn");
const staticUsername = document.getElementById("staticUsername");
const inputPassword = document.getElementById("inputPassword");
const testEmail = document.getElementById("testEmail");
const logoutBtn = document.getElementById("logout");

function bind() {
    loginBtn.onclick = () => {
        const username = staticUsername.value;
        const password = inputPassword.value;
        if (username === "" || password === "") {
            console.log("Input or password is empty");
            alert("Input or password is empty");
        } else {
            saveUsername = username;
            makeLoginRequest(username, password);
        }
    }

    testEmail.onclick = () => {
        makeEmailRequest(saveUsername);
    }

    logoutBtn.onclick = () => {
        makeLogoutRequest(saveUsername);
    }
}

function isStatusValid(status){
    return status === 200;
}

function makeLoginRequest(username, password) {
    const xhr = new XMLHttpRequest();

    xhr.open("POST", `${baseUrl}/login`);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify({
        username: username,
        password: password
    }));

    xhr.onload = function() {
        if (!isStatusValid(xhr.status)) {
            alert("Username oder Passwort ist falsch");
        } else {
            console.log("Login successful");
            document.cookie = xhr.responseText;
            console.log(document.cookie)
                //document.getElementById("testEmail").style.display="block";
                //document.getElementById("logout").style.display = "block";
            window.location.replace("http://127.0.0.1:5501/shop/Shop.html");
        }
    }
}

function makeEmailRequest(username) {
    const xhr = new XMLHttpRequest();

    xhr.open("GET", `${baseUrl}/email-data/${username}`);

    xhr.send();

    xhr.onload = function() {
        console.log(xhr.responseText);
    }
}

function makeLogoutRequest(username) {
    const xhr = new XMLHttpRequest();

    xhr.open("GET", `${baseUrl}/logout/${username}`);

    xhr.send();

    xhr.onload = function() {
        if (isStatusValid(xhr.status)) {
            console.log("Logout successful");
            testEmail.style.display = "none";
            logoutBtn.style.display = "none";
            document.cookie = "JSESSIONID=" + undefined;
        }
    }
}

testEmail.style.display = "none";
logoutBtn.style.display = "none";
bind();